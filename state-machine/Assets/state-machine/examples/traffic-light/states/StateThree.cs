﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateThree : State
{

    private void OnEnable()
    {
        StartCoroutine(SetLightsCoroutine());
    }

    private IEnumerator SetLightsCoroutine()
    {
        TrafficlightSystem lights = GetComponent<TrafficlightSystem>();
        lights.SetLightOne(true, false, false);
        lights.SetLightTwo(false, false, true);
        yield return new WaitForSeconds(3);
        SM.Push<StateFour>();
    }
}
