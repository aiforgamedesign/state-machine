﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFour : State
{

    private void OnEnable()
    {
        StartCoroutine(SetLightsCoroutine());
    }

    private IEnumerator SetLightsCoroutine()
    {
        TrafficlightSystem lights = GetComponent<TrafficlightSystem>();
        lights.SetLightOne(true, false, false);
        lights.SetLightTwo(false, true, false);
        yield return new WaitForSeconds(3);
        SM.Push<StateOne>();
    }
}
