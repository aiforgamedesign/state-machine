﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTwo : State
{

    private void OnEnable()
    {
        StartCoroutine(SetLightsCoroutine());
    }

    private IEnumerator SetLightsCoroutine()
    {
        TrafficlightSystem lights = GetComponent<TrafficlightSystem>();
        lights.SetLightOne(false, true, false);
        lights.SetLightTwo(true, false, false);
        yield return new WaitForSeconds(3);
        SM.Push<StateThree>();
    }
}
