﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficlightSystem : MonoBehaviour
{
    [Header("First traffic light")]
    public GameObject OneRed;
    public GameObject OneOrange;
    public GameObject OneGreen;
    [Header("Second traffic light")]
    public GameObject TwoRed;
    public GameObject TwoOrange;
    public GameObject TwoGreen;

    public void SetLightOne(bool red, bool orange, bool green)
    {
        OneRed.GetComponent<Renderer>().material.SetColor("_EmissionColor", red ? Color.red : Color.black);
        OneOrange.GetComponent<Renderer>().material.SetColor("_EmissionColor", orange ? Color.yellow : Color.black);
        OneGreen.GetComponent<Renderer>().material.SetColor("_EmissionColor", green ? Color.green : Color.black);
    }

    public void SetLightTwo(bool red, bool orange, bool green)
    {
        TwoRed.GetComponent<Renderer>().material.SetColor("_EmissionColor", red ? Color.red : Color.black);
        TwoOrange.GetComponent<Renderer>().material.SetColor("_EmissionColor", orange ? Color.yellow : Color.black);
        TwoGreen.GetComponent<Renderer>().material.SetColor("_EmissionColor", green ? Color.green : Color.black);
    }
}
