﻿using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public State[] States;
    protected Stack<State> stackHistory = new Stack<State>();

    private void Awake()
    {
        foreach(State state in States)
        {
            state.SM = this;
            state.enabled = false;
        }

        //Enable first State
        if (States.Length > 0)
        {
            States[0].enabled = true;
        }
    }

    public void Push<TState>() where TState: State
    {
        foreach(State state in States)
        {
            //Check if this was the previous state
            if (state.enabled)
            {
                //Push it to the stack
                stackHistory.Push(state);
            }

            //Disable all states
            state.enabled = false;
            if (state is TState)
            {
                //Enable the one we want
                state.enabled = true;
            }
        }
    }

    public void Pop()
    {
        foreach (State state in States)
        {
            //Disable all states
            state.enabled = false;
        }

        //Enable last state again
        State lastState = stackHistory.Pop();
        lastState.enabled = true;
    }


}
